import datetime
from PIL import Image
import os
import xlsxwriter

from flask import Flask, render_template, request
from flask_mysqldb import MySQL

app = Flask(__name__)

app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'panolatras'
app.config['MYSQL_HOST'] = 'localhost'

mysql = MySQL(app)

now = datetime.date.today()
today = now.strftime('%d%m%Y')


@app.route("/")
def index():
    return render_template('index.html')


@app.route("/file_explorer/<string:path>")
def file_explorer(path):
    arr_path = path.split('|')

    path_full = "/Users"
    arr_file = "<ul class='list-group list-group-flush'>"

    if len(arr_path) == 1:
        path_full = "/" + arr_path[0] + "/"
        arr_file += "<li class='list-group-item' rel='"+path_full+"'><a href='#'>..</a></li>"

    if len(arr_path) == 2:
        path_full = "/" + arr_path[0] + "/" + arr_path[1]
        arr_file += "<li class='list-group-item' rel='"+path_full+"'><a href='" + arr_path[0] + "'>..</a></li>"

    if len(arr_path) == 3:
        path_full = "/" + arr_path[0] + "/" + arr_path[1] + "/" + arr_path[2]
        arr_file += "<li class='list-group-item' rel='"+path_full+"'><a href='" + arr_path[0] + "|" + arr_path[1] + "'>..</a></li>"

    if len(arr_path) == 4:
        path_full = "/" + arr_path[0] + "/" + arr_path[1] + "/" + arr_path[2] + "/" + arr_path[3]
        arr_file += "<li class='list-group-item' rel='"+path_full+"'><a href='" + arr_path[0] + "|" + arr_path[1] + "|" + arr_path[2] + "'>..</a></li>"

    if len(arr_path) == 5:
        path_full = "/" + arr_path[0] + "/" + arr_path[1] + "/" + arr_path[2] + "/" + arr_path[3] + "/" + arr_path[4]
        arr_file += "<li class='list-group-item' rel='"+path_full+"'><a href='" + arr_path[0] + "|" + arr_path[1] + "|" + arr_path[2] + "|" + arr_path[3] + "'>..</a></li>"

    if len(arr_path) == 6:
        path_full = "/" + arr_path[0] + "/" + arr_path[1] + "/" + arr_path[2] + "/" + arr_path[3] + "/" + arr_path[4] + "/" + arr_path[5]
        arr_file += "<li class='list-group-item' rel='"+path_full+"'><a href='" + arr_path[0] + "|" + arr_path[1] + "|" + arr_path[2] + "|" + arr_path[3] + arr_path[4] + "'>..</a></li>"

    if len(arr_path) == 7:
        path_full = "/" + arr_path[0] + "/" + arr_path[1] + "/" + arr_path[2] + "/" + arr_path[3] + "/" + arr_path[4] + "/" + arr_path[5] + "/" + arr_path[6]
        arr_file += "<li class='list-group-item' rel='"+path_full+"'><a href='" + arr_path[0] + "|" + arr_path[1] + "|" + arr_path[2] + "|" + arr_path[3] + arr_path[4] + arr_path[5] + "'>..</a></li>"

    if len(arr_path) == 8:
        path_full = "/" + arr_path[0] + "/" + arr_path[1] + "/" + arr_path[2] + "/" + arr_path[3] + "/" + arr_path[4] + "/" + arr_path[5] + "/" + arr_path[6] + "/" + arr_path[7]
        arr_file += "<li class='list-group-item' rel='"+path_full+"'><a href='" + arr_path[0] + "|" + arr_path[1] + "|" + arr_path[2] + "|" + arr_path[3] + arr_path[4] + arr_path[5] + arr_path[6] + "'>..</a></li>"

    if len(arr_path) == 9:
        path_full = "/" + arr_path[0] + "/" + arr_path[1] + "/" + arr_path[2] + "/" + arr_path[3] + "/" + arr_path[4] + "/" + arr_path[5] + "/" + arr_path[6] + "/" + arr_path[7] + "/" + arr_path[8]
        arr_file += "<li class='list-group-item' rel='"+path_full+"'><a href='" + arr_path[0] + "|" + arr_path[1] + "|" + arr_path[2] + "|" + arr_path[3] + arr_path[4] + arr_path[5] + arr_path[6] + arr_path[7] + "'>..</a></li>"

    if len(arr_path) == 10:
        path_full = "/" + arr_path[0] + "/" + arr_path[1] + "/" + arr_path[2] + "/" + arr_path[3] + "/" + arr_path[4] + "/" + arr_path[5] + "/" + arr_path[6] + "/" + arr_path[7] + "/" + arr_path[8] + "/" + arr_path[9]
        arr_file += "<li class='list-group-item' rel='"+path_full+"'><a href='" + arr_path[0] + "|" + arr_path[1] + "|" + arr_path[2] + "|" + arr_path[3] + arr_path[4] + arr_path[5] + arr_path[6] + arr_path[7] + arr_path[8] + "'>..</a></li>"

    arr_dir = os.listdir(path_full)

    for file_name in arr_dir:
        if os.path.isdir(os.path.join(path_full, file_name)):
            arr_file += "<li class='list-group-item' rel='"+path_full+"/"+file_name+"'><a href='" + path + "|" + file_name + "'>" + file_name + "</a></li>"
        else:
            arr_file += "<li class='list-group-item'>" + file_name + "</li>"

    arr_file += "</ul>"

    return render_template('file_explorer.html', arr_file=arr_file)


@app.route('/generate_template', methods=['POST'])
def generate_template():
    conn = mysql.connection
    cursor = conn.cursor()

    template = Image.new("RGB", (8858, 28937), "white")

    block = 1
    template_id = 1

    total_images = 0
    images_printed = 0
    col_xls = 2

    path_input = request.form['path_input']
    path_output = request.form['path_output']

    text_file = open(path_output + "erros.txt", "w")

    workbook = xlsxwriter.Workbook(path_output + 'planilha_pedido_mosaico.xlsx')
    worksheet = workbook.add_worksheet()

    worksheet.write('A1', 'Pedido')
    worksheet.write('B1', 'Mosaico')

    os.chmod(path_input, 0777)
    os.chmod(path_output, 0777)

    for root, dirnames, filenames in os.walk(path_input):
        for filename in filenames:
            if filename.endswith(('.jpg', '.jpeg')):
                file_name = os.path.join(filename)

                arr_filename = file_name.split('_')

                prefix = arr_filename[0]
                material = arr_filename[1]
                demand = arr_filename[2]
                block_type = arr_filename[3]
                quantity = arr_filename[4].replace('x', '')
                product = arr_filename[5]

                query = "insert into panolatras.images " \
                        "(prefix, material, demand, block_type, quantity, product)" \
                        " values ('" + prefix + "', '" + material + "', '" + demand + "', '" + block_type + "', '" + quantity + "', '" + product + "') "

                cursor.execute(query)
                conn.commit()

                total_images += 1

    query = "select * from panolatras.images where template_id is null order by demand, quantity"
    cursor.execute(query)

    for file_ in cursor.fetchall():
        image_id = file_[0]
        prefix = file_[1]
        material = file_[2]
        demand = file_[3]
        block_type = file_[4]
        quantity = file_[5]
        product = file_[6]

        file_name = prefix + '_' + material + '_' + str(demand) + '_' + block_type + '_' + str(quantity) + 'x_' + product

        if block_type == 'Metro':
            quantity = 4 * quantity

        if block == 1:
            position = 0, 50

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 2:
            position = 4430, 50

            if quantity > 1:
                block += 1
                position = 0, 3230

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 3:
            position = 0, 3230

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 4:
            position = 4430, 3230

            if quantity > 1:
                block += 1
                position = 0, 6400

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 5:
            position = 0, 6400

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 6:
            position = 4430, 6400

            if quantity > 1:
                block += 1
                position = 0, 9570

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 7:
            position = 0, 9570

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 8:
            position = 4430, 9570

            if quantity > 1:
                block += 1
                position = 0, 12740

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 9:
            position = 0, 12740

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 10:
            position = 4430, 12740

            if quantity > 1:
                block += 1
                position = 0, 15910

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 11:

            if quantity <= 7:
                position = 0, 15910

                try:
                    img = Image.open(path_input + file_name).convert('RGBA')
                except IOError:
                    text_file.write("image file is truncated : %s" % file_name)

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

        elif block == 12:
            position = 4430, 15910

            if quantity <= 6:

                if quantity > 1:
                    block += 1
                    position = 0, 19080

                try:
                    img = Image.open(path_input + file_name).convert('RGBA')
                except IOError:
                    text_file.write("image file is truncated : %s" % file_name)

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

        elif block == 13:

            if quantity > 1:
                block += 1

            position = 0, 19080

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 14:
            position = 4430, 19080

            if quantity > 1:
                block += 1
                position = 0, 22250

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 15:
            if quantity < 4:
                position = 0, 22250

                try:
                    img = Image.open(path_input + file_name).convert('RGBA')
                except IOError:
                    text_file.write("image file is truncated : %s" % file_name)

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

        elif block == 16:
            position = 4430, 22250

            if quantity > 1:
                block += 1
                position = 0, 25420

            try:
                img = Image.open(path_input + file_name).convert('RGBA')
            except IOError:
                text_file.write("image file is truncated : %s" % file_name)

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 17:
            if quantity <= 2:
                position = 0, 25420

                try:
                    img = Image.open(path_input + file_name).convert('RGBA')
                except IOError:
                    text_file.write("image file is truncated : %s" % file_name)

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

            # template.save(path_output + "PD5m_" + today + "_" + str(template_id) + ".jpg", format="jpeg")

            # block = 1
            # template_id += 1
            # quantity = 0

            # template = Image.new("RGB", (8858, 28937), "white")

        elif block >= 18:
            if quantity == 1:
                position = 4430, 25420

                try:
                    img = Image.open(path_input + file_name).convert('RGBA')
                except IOError:
                    text_file.write("image file is truncated : %s" % file_name)

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

            template.save(path_output + "PD5m_" + today + "_" + str(template_id) + ".jpg", format="jpeg")

            block = 1
            template_id += 1
            quantity = 0

            template = Image.new("RGB", (8858, 28937), "white")

        block += quantity
        images_printed += 1

        worksheet.write('A' + str(col_xls), demand)
        worksheet.write('B' + str(col_xls), str(template_id))
        col_xls += 1

        if images_printed == total_images:
            template.save(path_output + "PD5m_" + today + "_" + str(template_id) + ".jpg", format="jpeg")

    workbook.close()
    text_file.close()

    # ---- remainder

    template = Image.new("RGB", (8858, 28937), "white")

    block = 1
    template_id += 1
    images_printed = 0
    col_xls = 2

    query = "select count(id) from images where template_id is null"
    cursor.execute(query)
    row_count = cursor.fetchone()
    count_template_null = row_count[0]

    workbook = xlsxwriter.Workbook(path_output + 'planilha_pedido_mosaico_sobra.xlsx')
    worksheet = workbook.add_worksheet()

    worksheet.write('A1', 'Pedido')
    worksheet.write('B1', 'Mosaico')

    os.chmod(path_input, 0777)
    os.chmod(path_output, 0777)

    query = "select * from panolatras.images where template_id is null order by demand, quantity"
    cursor.execute(query)

    for file_ in cursor.fetchall():
        image_id = file_[0]
        prefix = file_[1]
        material = file_[2]
        demand = file_[3]
        block_type = file_[4]
        quantity = file_[5]
        product = file_[6]

        file_name = prefix + '_' + material + '_' + str(demand) + '_' + block_type + '_' + str(quantity) + 'x_' + product

        if block_type == 'Metro':
            quantity = 4 * quantity

        if block == 1:
            position = 0, 50
            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 2:
            position = 4430, 50

            if quantity > 1:
                block += 1
                position = 0, 3230

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 3:
            position = 0, 3230
            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 4:
            position = 4430, 3230

            if quantity > 1:
                block += 1
                position = 0, 6400

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 5:
            position = 0, 6400
            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 6:
            position = 4430, 6400

            if quantity > 1:
                block += 1
                position = 0, 9570

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 7:
            position = 0, 9570

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 8:
            position = 4430, 9570

            if quantity > 1:
                block += 1
                position = 0, 12740

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 9:
            position = 0, 12740

            if quantity > 1:
                block += 1

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 10:
            position = 4430, 12740

            if quantity > 1:
                block += 1
                position = 0, 15910
            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 11:

            if quantity <= 7:
                position = 0, 15910
                img = Image.open(path_input + file_name).convert('RGBA')

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

        elif block == 12:
            position = 4430, 15910

            if quantity <= 6:

                if quantity > 1:
                    block += 1
                    position = 0, 19080

                img = Image.open(path_input + file_name).convert('RGBA')

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

        elif block == 13:

            if quantity > 1:
                block += 1

            position = 0, 19080
            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 14:
            position = 4430, 19080

            if quantity > 1:
                block += 1
                position = 0, 22250

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 15:
            if quantity <= 4:
                position = 0, 22250
                img = Image.open(path_input + file_name).convert('RGBA')

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

        elif block == 16:
            position = 4430, 22250

            if quantity > 1:
                block += 1
                position = 0, 25420

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 17:
            if quantity <= 2:
                position = 0, 25420

                img = Image.open(path_input + file_name).convert('RGBA')

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

            # template.save(path_output + "PD5m_" + today + "_" + str(template_id) + ".jpg", format="jpeg")

            # block = 1
            # template_id += 1
            # quantity = 0

            template = Image.new("RGB", (8858, 28937), "white")

        elif block >= 18:
            if quantity == 1:
                position = 4430, 25420

                img = Image.open(path_input + file_name).convert('RGBA')

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

            template.save(path_output + "PD5m_" + today + "_" + str(template_id) + ".jpg", format="jpeg")

            block = 1
            template_id += 1
            quantity = 0

            template = Image.new("RGB", (8858, 28937), "white")

        block += quantity
        images_printed += 1

        worksheet.write('A' + str(col_xls), demand)
        worksheet.write('B' + str(col_xls), str(template_id))
        col_xls += 1

        if images_printed == count_template_null:
            template.save(path_output + "PD5m_" + today + "_" + str(template_id) + ".jpg", format="jpeg")

    workbook.close()

    # ---- end remainder

    # ---- remainder 2

    template = Image.new("RGB", (8858, 28937), "white")

    block = 1
    template_id += 1
    images_printed = 0
    col_xls = 2

    query = "select count(id) from images where template_id is null"
    cursor.execute(query)
    row_count = cursor.fetchone()
    count_template_null = row_count[0]

    workbook = xlsxwriter.Workbook(path_output + 'planilha_pedido_mosaico_sobra2.xlsx')
    worksheet = workbook.add_worksheet()

    worksheet.write('A1', 'Pedido')
    worksheet.write('B1', 'Mosaico')

    os.chmod(path_input, 0777)
    os.chmod(path_output, 0777)

    query = "select * from panolatras.images where template_id is null order by demand, quantity"
    cursor.execute(query)

    for file_ in cursor.fetchall():
        image_id = file_[0]
        prefix = file_[1]
        material = file_[2]
        demand = file_[3]
        block_type = file_[4]
        quantity = file_[5]
        product = file_[6]

        file_name = prefix + '_' + material + '_' + str(demand) + '_' + block_type + '_' + str(quantity) + 'x_' + product

        if block_type == 'Metro':
            quantity = 4 * quantity

        if block == 1:
            position = 0, 50
            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 2:
            position = 4430, 50

            if quantity > 1:
                block += 1
                position = 0, 3230

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 3:
            position = 0, 3230
            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 4:
            position = 4430, 3230

            if quantity > 1:
                block += 1
                position = 0, 6400

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 5:
            position = 0, 6400
            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 6:
            position = 4430, 6400

            if quantity > 1:
                block += 1
                position = 0, 9570

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 7:
            position = 0, 9570

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 8:
            position = 4430, 9570

            if quantity > 1:
                block += 1
                position = 0, 12740

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 9:
            position = 0, 12740
            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 10:
            position = 4430, 12740

            if quantity > 1:
                block += 1
                position = 0, 15910
            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 11:

            if quantity <= 7:
                position = 0, 15910
                img = Image.open(path_input + file_name).convert('RGBA')

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

        elif block == 12:
            position = 4430, 15910

            if quantity <= 6:

                if quantity > 1:
                    block += 1
                    position = 0, 19080

                img = Image.open(path_input + file_name).convert('RGBA')

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

        elif block == 13:

            if quantity > 1:
                block += 1

            position = 0, 19080
            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 14:
            position = 4430, 19080

            if quantity > 1:
                block += 1
                position = 0, 22250

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 15:
            if quantity <= 4:
                position = 0, 22250
                img = Image.open(path_input + file_name).convert('RGBA')

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

        elif block == 16:
            position = 4430, 22250

            if quantity > 1:
                block += 1
                position = 0, 25420

            img = Image.open(path_input + file_name).convert('RGBA')

            template.paste(img, position)

            query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
            cursor.execute(query)
            conn.commit()

        elif block == 17:
            if quantity <= 2:
                position = 0, 25420

                img = Image.open(path_input + file_name).convert('RGBA')

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

            # template.save(path_output + "PD5m_" + today + "_" + str(template_id) + ".jpg", format="jpeg")

            # block = 1
            # template_id += 1
            # quantity = 0

            template = Image.new("RGB", (8858, 28937), "white")

        elif block >= 18:
            if quantity == 1:
                position = 4430, 25420

                img = Image.open(path_input + file_name).convert('RGBA')

                template.paste(img, position)

                query = "update panolatras.images set template_id = " + str(template_id) + " where id = " + str(image_id)
                cursor.execute(query)
                conn.commit()

            template.save(path_output + "PD5m_" + today + "_" + str(template_id) + ".jpg", format="jpeg")

            block = 1
            template_id += 1
            quantity = 0

            template = Image.new("RGB", (8858, 28937), "white")

        block += quantity
        images_printed += 1

        worksheet.write('A' + str(col_xls), demand)
        worksheet.write('B' + str(col_xls), str(template_id))
        col_xls += 1

        if images_printed == count_template_null:
            template.save(path_output + "PD5m_" + today + "_" + str(template_id) + ".jpg", format="jpeg")

    workbook.close()

    # ---- end remainder 2

    return render_template('done.html')


@app.errorhandler(400)
def bad_request(e):
    return render_template('error_400.html'), 400


@app.errorhandler(404)
def not_found(e):
    return render_template('error_404.html'), 404


@app.errorhandler(403)
def forbidden(e):
    return render_template('error_403.html'), 403


@app.errorhandler(410)
def gone(e):
    return render_template('error_410.html'), 410


@app.errorhandler(500)
def internal_server_error(e):
    return render_template('error_500.html'), 500
